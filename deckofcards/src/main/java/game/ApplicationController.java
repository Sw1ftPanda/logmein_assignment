package game;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApplicationController {
	
	private static ApplicationController instance;
	private HashMap<Integer, Game> games = new HashMap<>();
	private HashMap<Integer, Deck> decks = new HashMap<>();
	private HashMap<Integer, Player> players = new HashMap<>();
	
	private ApplicationController() {
		
	}
	
	public static ApplicationController getInstance() {
		if(instance == null) instance = new ApplicationController();
		return instance;
	}

	public HashMap<Integer, Game> getGames() { return games; }
	public HashMap<Integer, Deck> getDecks() { return decks; }
	public HashMap<Integer, Player> getPlayers() { return players; }

	public Game getGame(int id) {
		if(games.containsKey(id)) return games.get(id);
		return null;
	}
	
	public void removeGame(int id) {
		if(games.containsKey(id)) games.remove(id);
	}
	
	public Deck getDeck(int id) {
		if(decks.containsKey(id)) return decks.get(id);
		return null;
	}
	
	public void removeDeck(int id) {
		if(decks.containsKey(id)) decks.remove(id);
	}
	
	public Player getPlayer(int id) {
		if(players.containsKey(id)) return players.get(id);
		return null;
	}
	
	public void removePlayer(int id) {
		if(players.containsKey(id)) players.remove(id);
	}
	
	@Override
	public String toString() {
		return this.getJson().toString();
	}
	
	public JSONObject getJson() {
		JSONArray games_array = new JSONArray();
		Iterator<Map.Entry<Integer, Game>> iterator = games.entrySet().iterator();
        while (iterator.hasNext()) {
             Map.Entry<Integer, Game> entry = (Map.Entry<Integer, Game>) iterator.next();
             games_array.put(((Game)entry.getValue()).getJson());
        }
        JSONObject obj = new JSONObject();
        obj.put("games", games_array);
        return obj;
	}

}
