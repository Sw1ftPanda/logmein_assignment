package game;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Player implements Serializable {
	
	private static final long serialVersionUID = 8741011654577539616L;
	private static int auto_index = 1;
	
	private int id;
	private ArrayList<Card> cards = new ArrayList<>();
	//private String name = "Tommy";
	
	public Player() {
		this.id = auto_index;
		auto_index++;
	}
	
	public int getId() { return this.id; }
	
	public void giveCard(Card card) {
		this.cards.add(card);
	}
	
	public ArrayList<Card> getCards(){
		return this.cards;
	}
	
	protected int getScore() {
		int score = 0;
		for(Card card : this.cards) {
			score += card.getValue().getFaceValue();
		}
		return score;
	}
	
	@Override
	public String toString() {
		return this.getJson().toString();
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", this.id);
		obj.put("name", "Player " + this.id);
		JSONArray cards_array = new JSONArray();
		for(Card card : this.cards) {
			cards_array.put(card.getJson());
		}
		obj.put("cards", cards_array);
		obj.put("total_added_value", this.getScore());
		return obj;
	}

}
