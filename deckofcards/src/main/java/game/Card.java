package game;

import org.json.JSONObject;

enum CardValue {
	KING(13),
	QUEEN(12),
	JACK(11),
	TEN(10),
	NINE(9),
	EIGHT(8),
	SEVEN(7),
	SIX(6),
	FIVE(5),
	FOUR(4),
	THREE(3),
	TWO(2),
	ACE(1);
	
	private int face_value;
	
	public int getFaceValue() {
		return face_value;
	}
	
	private CardValue(int face_value) {
		this.face_value = face_value;
	}
}

enum CardSuit {
	HEARTS,
	SPADES,
	CLUBS,
	DIAMONDS
}

public class Card {
	
	private CardValue value;
	private CardSuit suit;
	
	public Card(CardValue value, CardSuit suit) {
		this.value = value;
		this.suit = suit;
	}

	public CardValue getValue() {
		return value;
	}

	public CardSuit getSuit() {
		return suit;
	}
	
	public int compareTo(Card card) {
		if(this.suit.ordinal() < card.suit.ordinal()) {
			return -1;
		} else if(this.suit.ordinal() == card.suit.ordinal()) {
			if(this.value.getFaceValue() > card.value.getFaceValue()) {
				return -1;
			} else if(this.value.getFaceValue() == card.value.getFaceValue()) {
				return 0;
			}
		}
		return 1;
	}
	
	@Override
	public String toString() {
		return this.getJson().toString();
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("value", this.value.toString());
		obj.put("suit", this.suit.toString());
		return obj;
	}
}
