package game;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Game implements Serializable {
	
	private static final long serialVersionUID = -6624517380034181437L;
	private static int auto_index = 1; 
	
	private int id;
	private Deck shoe;
	private ArrayList<Player> players;
	
	public Game() {
		this.id = auto_index;
		auto_index++;
		shoe = new Deck(DeckState.EMPTY);
		players = new ArrayList<>();
	}
	
	public int getId() {
		return this.id;
	}
	
	public ArrayList<Player> getPlayers(){
		return this.players;
	}
	
	public Player getPlayer(int playerId) {
		for(Player player : players) {
			if(player.getId() == playerId) return player;
		}
		return null;
	}
	
	public void addPlayer(Player player) {
		players.add(player);
	}
	
	public void removePlayer(Player player) {
		if(players.contains(player)) players.remove(player);
	}

	public void addDeck(Deck deck) {
		shoe.add(deck);
	}
	
	public Deck getShoe() {
		return this.shoe;
	}
	
	public boolean dealCards(int amount, int playerId) {
		Player player = this.getPlayer(playerId);
		if(player == null) return false;
		
		for(int i = 0; i < amount; i++) {
			Card card = this.shoe.dealCard();
			if(card != null) {
				player.giveCard(card);
			} else {
				break;
			}
		}
		return true;
	}
	
	@Override
	public String toString() {
		return this.getJson().toString();
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", this.id);
		JSONArray players_array = new JSONArray();
		for(Player player : this.players) {
			players_array.put(player.getJson());
		}
		obj.put("players", players_array);
		obj.put("undealt", shoe.getUndealt());
		return obj;
	}
	
}
