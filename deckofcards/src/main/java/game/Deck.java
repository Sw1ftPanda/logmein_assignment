package game;

import java.io.Serializable;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;

enum DeckState {
	EMPTY,
	FULL
}

public class Deck implements Serializable {
	
	private static final long serialVersionUID = 8958327360141753692L;
	private static int auto_index = 1;
		
	private int id;
	private ArrayList<Card> cards = new ArrayList<>();
	
	public Deck() {
		this(DeckState.FULL);
	}
	
	public Deck(DeckState state) {
		this.id = auto_index;
		auto_index++;
		if(state == DeckState.FULL) this.createCards();
	}

	private void createCards() {
		for(CardSuit suit: CardSuit.values()) {
			for(CardValue value: CardValue.values()) {
				cards.add(new Card(value, suit));
			}
		}
	}

	public int getId() {
		return id;
	}
	
	protected ArrayList<Card> getCards() { 
		return cards;
	}
	
	public void shuffle() {
		ArrayList<Card> shuffled_deck = new ArrayList<>();
		
		for(Card card : this.cards) {
			int new_index = (int)(Math.random() * shuffled_deck.size());
			shuffled_deck.add(new_index, card);
		}
		
		if(shuffled_deck.size() != this.cards.size()) {
			System.out.println("Shuffling error occured");
		} else {
			this.cards = shuffled_deck;
		}
		System.out.println("Shuffled deck!");
	}
	
	public Card dealCard() {
		if(this.cards.size() > 0) {
			Card card = this.cards.get(0);
			this.cards.remove(0);
			return card;
		}
		return null;
	}
	
	public void add(Deck deck) {
		cards.addAll(deck.getCards());
	}
	
	public JSONObject getUndealt() {
		JSONObject undealt = new JSONObject();
		int hearts = 0, spades = 0, clubs = 0, diamonds = 0;
		
		for(Card card : cards) {
			switch(card.getSuit()) {
			case HEARTS: hearts++; break;
			case SPADES: spades++; break;
			case CLUBS: clubs++; break;
			case DIAMONDS: diamonds++; break;
			default: break;
			}
		}
		
		undealt.put("hearts", hearts);
		undealt.put("spades", spades);
		undealt.put("clubs", clubs);
		undealt.put("diamonds", diamonds);
		return undealt;
	}
	
	protected ArrayList<Card> getOrderedCards(){
		ArrayList<Card> ordered_cards = new ArrayList<>();
		
		for(Card card : this.cards) {
			for(int i = 0; i < ordered_cards.size(); i++) {
				if(ordered_cards.get(i).compareTo(card) >= 0) {
					ordered_cards.add(i, card);
					break;
				} else if(i == ordered_cards.size()-1) {
					ordered_cards.add(card);
					break;
				}
			}
			// Initial conditions
			if(ordered_cards.size() == 0) {
				ordered_cards.add(card);
			}
		}
		return ordered_cards;
	}

	@Override
	public String toString() {
		return this.getJson().toString();
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", this.id);
		JSONArray cards_array = new JSONArray();
		for(Card card : this.getOrderedCards()) {
			cards_array.put(card.getJson());
		}
		obj.put("cards", cards_array);
		return obj;
	}

}
