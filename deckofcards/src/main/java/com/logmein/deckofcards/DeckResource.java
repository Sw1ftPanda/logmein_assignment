package com.logmein.deckofcards;

import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import game.*;

@Path("/decks")
public class DeckResource {
	
	private static ApplicationController controller = ApplicationController.getInstance();
	
	/**
	 * RETRIEVES ALL UNASSIGNED DECKS
	 * POST /deckofcards/rest/decks/
	 * 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readAllDecks() {
		JSONArray decks_array = new JSONArray();
		Iterator<Map.Entry<Integer, Deck>> iterator = controller.getDecks().entrySet().iterator();
        while (iterator.hasNext()) {
             Map.Entry<Integer, Deck> entry = (Map.Entry<Integer, Deck>) iterator.next();
             decks_array.put(((Deck)entry.getValue()).getJson());
        }
        JSONObject obj = new JSONObject();
        obj.put("decks", decks_array);
        return obj.toString();
	}
	
	/**
	 * CREATE A NEW DECK
	 * POST /deckofcards/rest/decks/
	 * 
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String createDeck() {
		Deck deck = new Deck();
		controller.getDecks().put(deck.getId(), deck);
		return deck.toString();
	}
	
	/**
	 * RETRIEVE AN UNASSIGNED DECK
	 * GET /deckofcards/rest/decks/{playerId}
	 * 
	 * @param deckId
	 * @return
	 */
	@Path("/{deckId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readPlayer(@PathParam("deckId") int deckId) {
		Deck deck = controller.getDeck(deckId);
		if(deck != null) return deck.toString();
		return "{}";
	}

}
