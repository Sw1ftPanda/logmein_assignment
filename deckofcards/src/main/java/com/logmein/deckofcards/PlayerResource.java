package com.logmein.deckofcards;

import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import game.*;

@Path("/players")
public class PlayerResource {
	
	private static ApplicationController controller = ApplicationController.getInstance();
	
	/**
	 * RETRIEVES ALL PLAYERS
	 * POST /deckofcards/rest/players/
	 * 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readAllPlayers() {
		JSONArray players_array = new JSONArray();
		Iterator<Map.Entry<Integer, Player>> iterator = controller.getPlayers().entrySet().iterator();
        while (iterator.hasNext()) {
             Map.Entry<Integer, Player> entry = (Map.Entry<Integer, Player>) iterator.next();
             players_array.put(((Player)entry.getValue()).getJson());
        }
        JSONObject obj = new JSONObject();
        obj.put("players", players_array);
        return obj.toString();
	}
	
	/**
	 * CREATE A NEW PLAYER
	 * POST /deckofcards/rest/players/
	 * 
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String createPlayer() {
		Player player = new Player();
		controller.getPlayers().put(player.getId(), player);
		return player.toString();
	}
	
	/**
	 * RETRIEVE AN UNASSIGNED PLAYER
	 * GET /deckofcards/rest/players/{playerId}
	 * 
	 * @param playerId
	 * @return
	 */
	@Path("/{playerId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readPlayer(@PathParam("playerId") int playerId) {
		Player player = controller.getPlayer(playerId);
		if(player != null) return player.toString();
		return "{}";
	}


}
