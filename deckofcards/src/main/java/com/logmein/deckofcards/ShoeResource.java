package com.logmein.deckofcards;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import game.*;

@Path("/games/{gameId}/shoe")
public class ShoeResource {
	
	private static ApplicationController controller = ApplicationController.getInstance();
	
	/**
	 * RETRIEVE ALL CARDS FROM THE GAME'S DECK (SHOE)
	 * GET /deckofcards/rest/games/{gameId}/shoe
	 * 
	 * @return {String}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readShoe(@PathParam("gameId") int gameId) {
		return controller.getGame(gameId).getShoe().toString();
    }
	
	/**
	 * SHUFFLE THE GAME DECK OR DEAL A CARD TO A PLAYER
	 * POST /deckofcards/rest/games/{gameId}/shoe
	 * 
	 * SHUFFLE {"action":"shuffle"}
	 * 
	 * DEAL { "action":"deal", "amount":<amount>, "playerId":<playerId> }
	 * 
	 * @return {String}
	 */
	@POST
	//@Consumes(MediaType.APPLICATION_JSON)
	public String actionOnShoe(String request, @PathParam("gameId") int gameId) throws Exception{
		try {
			JSONObject jsonObject = new JSONObject(request);
			System.out.println("gameId: " + gameId);
			String action = (String)jsonObject.get("action");
			System.out.println("action: " + action);
			
			if(action.equals("shuffle")) {
				Game game = controller.getGame(gameId);
				if(game != null) game.getShoe().shuffle();
			} else if(action.equals("deal")) {
				int playerId = (int)jsonObject.get("playerId");
				int amount = 1;
				if(jsonObject.has("amount")) {
					amount = (int)jsonObject.get("amount");
				}
				
				Game game = controller.getGame(gameId);
				if(game != null) {
					
					game.dealCards(amount, playerId);
				}				
			}
			//System.out.println("amount: " + action.amount);
			//System.out.println("playerId: " + action.playerId);

		} catch(ClassCastException e){
			e.getStackTrace();
			return "Casting error";
		} catch(JSONException e){
			e.getStackTrace();
			return "Error";
		}
		
		return controller.getGame(gameId).getShoe().toString();
    }

}


