package com.logmein.deckofcards;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import game.*;

@Path("/games")
public class GameResource {
	
	private static ApplicationController controller = ApplicationController.getInstance();
	
	/**
	 * RETRIEVE ALL GAMES
	 * GET /deckofcards/rest/games/
	 * 
	 * @return {String}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readAllGames() {
		return controller.toString();
    }
	
	/**
	 * CREATE A NEW GAME
	 * POST /deckofcards/rest/games/
	 * 
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String createGame() {
		Game game = new Game();
		controller.getGames().put(game.getId(), game);
		return game.toString();
	}
	
	/**
	 * RETRIEVE A GAME
	 * GET /deckofcards/rest/games/{gameId}
	 * 
	 * @param gameId
	 * @return
	 */
	@Path("/{gameId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readGame(@PathParam("gameId") int gameId) {
		Game game = controller.getGame(gameId);
		if(game != null) return game.toString();
		return "{}";
    }
	
	/**
	 * REMOVE A GAME
	 * DELETE /deckofcards/rest/games/{gameId}
	 * 
	 * @param gameId
	 * @return
	 */
	@Path("/{gameId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void removeGame(@PathParam("gameId") int gameId) {
		//Remove all players
		Game game = controller.getGame(gameId);
		if(game != null) {	
			for(Player player: game.getPlayers()) {
				controller.getPlayers().put(player.getId(), player);
			}
		}		
		controller.removeGame(gameId);
    }
	
	
	/**
	 * ADD A DECK TO A GAME'S SHOE
	 * POST /deckofcards/rest/games/{gameId}/decks/{deckId}
	 * 
	 * @param gameId
	 * @param deckId
	 * @return
	 */
	@Path("/{gameId}/decks/{deckId}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String addDeck(@PathParam("gameId") int gameId, @PathParam("deckId") int deckId) {
		Game game = controller.getGame(gameId);
		Deck deck = controller.getDeck(deckId);
		if(game != null && deck != null) {
			controller.removeDeck(deckId);
			game.addDeck(deck);
			return game.toString();
		}
		return "{}";
    }
	
	/**
	 * ADD A PLAYER TO A GAME
	 * POST /deckofcards/rest/games/{gameId}/players/{playerId}
	 * 
	 * @param gameId
	 * @param playerId
	 * @return
	 */
	@Path("/{gameId}/players/{playerId}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String addPlayer(@PathParam("gameId") int gameId, @PathParam("playerId") int playerId) {
		Game game = controller.getGame(gameId);
		Player player = controller.getPlayer(playerId);
		if(game != null && player != null) {
			game.addPlayer(player);
			controller.removePlayer(playerId);
			return game.toString();
		}
		return "{}";
    }
	
	/**
	 * RETRIEVE A PLAYER
	 * GET /deckofcards/rest/games/{gameId}/players/{playerId}
	 * 
	 * @param gameId
	 * @param playerId
	 * @return
	 */
	@Path("/{gameId}/players/{playerId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String readPlayer(@PathParam("gameId") int gameId, @PathParam("playerId") int playerId) {
		Game game = controller.getGame(gameId);
		Player player = game.getPlayer(playerId);
		if(game != null && player != null) {
			return player.toString();
		}
		return "{}";
	}
	
	/**
	 * REMOVE A PLAYER FROM A GAME
	 * DELETE /deckofcards/rest/games/{gameId}/players/{playerId}
	 * 
	 * @param gameId
	 * @param playerId
	 * @return
	 */
	@Path("/{gameId}/players/{playerId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public String removePlayer(@PathParam("gameId") int gameId, @PathParam("playerId") int playerId) {
		Game game = controller.getGame(gameId);
		Player player = game.getPlayer(playerId);
		if(game != null && player != null) {
			game.removePlayer(player);
			controller.getPlayers().put(player.getId(), player);
			return game.toString();
		}
		return "{}";
    }

}
