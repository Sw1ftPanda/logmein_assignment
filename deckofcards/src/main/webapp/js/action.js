
var games_table, decks_table, players_table, players_game, game_info;


const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const gameId = urlParams.get('gameId');
console.log(gameId)

$(document).ready(function() {
	
	var href = window.location.href;
	if(href.match(/game.html/)){
		game_info = $('#game-info').DataTable({
			"ajax": {
		        "url": "/deckofcards/rest/games/",
		        "dataSrc": "games"
		    },
		    "columns": [
		    	{ 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<span>'+data+'</span>&nbsp;<button class="btn btn-danger float-right" onclick="deleteGame('+data+')">Remove</button>';
		            }
	            },
	            { 
	            	"data": "players",
	            	"render": function ( data, type, row, meta ) {
	            		var result = '<ul class="list-unstyled">';
	            		for(var player of data){
	            			result += '<li><span>'+player.name+'</span>&nbsp;<button class="btn btn-danger btn-sm ml-3 mb-1" onclick="removePlayerFromGame('+row.id+','+player.id+')">Remove</button></li>'
	            		}
		                return result + '</ul>';
		            }
	            },
	            { 
	            	"data": "undealt", 
	            	"render": function ( data, type, row, meta ) {
		                return '<ul class="list-unstyled"><li><b>Hearts</b>: '+data.hearts+'</li><li><b>Spades</b>: '+data.spades+'</li><li><b>Clubs</b>: '+data.clubs+'</li><li><b>Diamonds</b>: '+data.diamonds+'</li></ul>';
		            }
	            },
	            { 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<a class="btn btn-secondary" href="/deckofcards/rest/games/'+data+'/shoe">View shoe</a>';
		            }
	            },
	            { 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<button class="btn btn-secondary" onclick="shuffleDeck('+data+')">Shuffle</button>';
		            }
	            }
	        ],
	        "searchCols": [
	        	{ "search": gameId, "escapeRegex": false },
	            null,
	            null,
	            null,
	            null	            
	          ]
		});
		
		players_game = $('#players-game').DataTable({
			"ajax": {
		        "url": "/deckofcards/rest/games/"+gameId,
		        "dataSrc": "players"
		    },
		    "columns": [
		    	{ 
	            	"data": "name",
	            	"render": function ( data, type, row, meta ) {
		                return row.id+' - '+data;
		            }
	            },
	            { 
	            	"data": "cards",
	            	"render": function ( data, type, row, meta ) {
	            		var result = '<ul class="list-unstyled">';
	            		for(var card of data){
	            			result += '<li><span>'+card.value+' OF '+card.suit+'</span></li>'
	            		}
		                return result + '</ul>';
		            }
	            },
	            { "data": "total_added_value" },
	            { 
	            	"data": "id",
	            	"render": function ( data, type, row, meta ) {
		                return '<a class="btn btn-secondary" href="/deckofcards/rest/games/'+gameId+'/players/'+data+'">View</a>';
		            }
	            },
	            { 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<div class="form-inline"><input id="cards-'+data+'" class="form-control mr-1" type="text" placeholder="Amount" maxlength="4" size="4"><button class="btn btn-secondary" onclick="dealCardsToPlayer('+gameId+','+data+', $(\'#cards-'+data+'\').val())">Deal cards</button></div>';
		            }
	            }
	        ]
		});
		
	} else {
		games_table = $('#games-table').DataTable({
			"ajax": {
		        "url": "/deckofcards/rest/games/",
		        "dataSrc": "games"
		    },
		    "columns": [
		    	{ 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<span>'+data+'</span>&nbsp;<button class="btn btn-danger float-right" onclick="deleteGame('+data+')">Remove</button>';
		            }
	            },
	            { 
	            	"data": "players",
	            	"render": function ( data, type, row, meta ) {
	            		var result = '<ul class="list-unstyled">';
	            		for(var player of data){
	            			result += '<li><span>'+player.name+'</span>&nbsp;<button class="btn btn-danger btn-sm ml-3 mb-1" onclick="removePlayerFromGame('+row.id+','+player.id+')">Remove</button></li>'
	            		}
		                return result + '</ul>';
		            }
	            },
	            { 
	            	"data": "undealt", 
	            	"render": function ( data, type, row, meta ) {
		                return '<ul class="list-unstyled"><li><b>Hearts</b>: '+data.hearts+'</li><li><b>Spades</b>: '+data.spades+'</li><li><b>Clubs</b>: '+data.clubs+'</li><li><b>Diamonds</b>: '+data.diamonds+'</li></ul>';
		            }
	            },
	            { 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<a class="btn btn-secondary" href="/deckofcards/rest/games/'+data+'">View</a>';
		            }
	            },
	            { 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<div class="form-inline"><input id="deck-'+data+'" class="form-control mr-1" type="text" placeholder="Deck ID" maxlength="4" size="4"><button class="btn btn-secondary" onclick="addDeckToGame('+data+', $(\'#deck-'+data+'\').val())">Add deck</button></div>';
		            }
	            },
	            { 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<button class="btn btn-secondary" onclick="shuffleDeck('+data+')">Shuffle</button><div class="clearfix"></div><a class="btn btn-primary mt-2" href="/deckofcards/game.html?gameId='+data+'">Game Interface</a>';
		            }
	            }
	        ]
		});
		
		decks_table = $('#decks-table').DataTable({
			"ajax": {
		        "url": "/deckofcards/rest/decks/",
		        "dataSrc": "decks"
		    },
		    "columns": [
	            { "data": "id" },
	            { 
	            	"data": "cards",
	            	"render": function ( data, type, row, meta ) {
	            		return data.length;
		            }
	            },
	            { 
	            	"data": "id", 
	            	"render": function ( data, type, row, meta ) {
		                return '<a class="btn btn-secondary" href="/deckofcards/rest/decks/'+data+'">View</a>';
		            }
	            }
	        ]
		});
		
		players_table = $('#players-table').DataTable({
			"ajax": {
		        "url": "/deckofcards/rest/players/",
		        "dataSrc": "players"
		    },
		    "columns": [
	            { "data": "id" },
	            { "data": "name" },
	            { 
	            	"data": "id",
	            	"render": function ( data, type, row, meta ) {
		                return '<a class="btn btn-secondary" href="/deckofcards/rest/players/'+data+'">View</a>';
		            }
	            },
	            { 
	            	"data": "id",
	            	"render": function ( data, type, row, meta ) {
		                return '<div class="form-inline"><input id="game-'+data+'" class="form-control mr-1" type="text" placeholder="Game ID" maxlength="4" size="4"><button class="btn btn-secondary" onclick="addPlayerToGame($(\'#game-'+data+'\').val(),'+data+')">Assign to game</button></div>';
		            }
	            }
	        ]
		});
	}
});

function post(url, json){
	packaged_json = "";
	if(json) packaged_json = JSON.stringify(json);
	$.post(url, packaged_json, function(data, status){
		if(status !== 'success') {
			alert("Data: " + data + "\nStatus: " + status)
			return
		}
		
		if(data && data.id)	{
			//window.open("/deckofcards/rest/games/" + data.id, "_blank")
			refresh_tables()
		} else {
			alert('Unknown error occured!\n' + data)
		}
	}, 'json');
}

function del(url){
	$.ajax({
	   "url": url,
	   "type": 'DELETE',
	   "success": function(response) {
		   refresh_tables()
	   },
	   "error": function(err){
		   alert(err);
	   }
	});
}

function refresh_tables(){
	if(games_table) games_table.ajax.reload()
	if(decks_table) decks_table.ajax.reload()
	if(players_table) players_table.ajax.reload()
	if(players_game) players_game.ajax.reload()
	if(game_info) game_info.ajax.reload()
}

function createGame() {	post("/deckofcards/rest/games", "") }

function deleteGame(game_id) { del("/deckofcards/rest/games/"+game_id) }

function createDeck() {	post("/deckofcards/rest/decks", "") }

function createPlayer() { post("/deckofcards/rest/players", "") }

function removePlayerFromGame(game_id, player_id) { del("/deckofcards/rest/games/"+game_id+"/players/"+player_id) }

function addDeckToGame(game_id, deck_id) { /*alert('game_id: '+game_id+' deck_id: '+deck_id);*/ post("/deckofcards/rest/games/"+game_id+"/decks/"+deck_id, "") }

function addPlayerToGame(game_id, player_id) { post("/deckofcards/rest/games/"+game_id+"/players/"+player_id, "") }

function shuffleDeck(game_id) {
	post("/deckofcards/rest/games/"+game_id+"/shoe", {"action":"shuffle"});
}

function dealCardsToPlayer(game_id, player_id, amount) {
	post("/deckofcards/rest/games/"+game_id+"/shoe", {"action":"deal", "amount":parseInt(amount), "playerId":parseInt(player_id)});
}
