# A Basic Deck of Cards Game

## Setup
* In Eclipse -> Import... -> Maven -> Existing Maven Projects
* Navigate to directory 'deckofcards' of this project
* Right-click project -> Maven -> Update Project...
* Configure the Tomcat Server
    * File -> New -> Other... -> Server
    * Add... (if no environment is setup)
    * Finish
* Run -> Run on Server -> Finish

The testing dashboard's main link is : http://localhost:8080/deckofcards/index.html

## Versions
- Eclipse IDE for Enterprise (Version: 2020-03 (4.15.0))
- Apache Tomcat 9.0.33
- OpenJDK 14 (Java SE 1.8)

## REST API

| Description                       | URL                                                 | Method | Data                                                          | Response                                     |
|-----------------------------------|-----------------------------------------------------|--------|---------------------------------------------------------------|----------------------------------------------|
| Retrieve all games                | /deckofcards/rest/games/                            | GET    | -                                                             | List of games                                |
| Create a new game                 | /deckofcards/rest/games/                            | POST   | -                                                             | The new game                                 |
| Retrieve a game                   | /deckofcards/rest/games/{gameId}                    | GET    | -                                                             | A game                                       |
| Delete a game                     | /deckofcards/rest/games/{gameId}                    | DELETE | -                                                             | -                                            |
| Add a deck to a game              | /deckofcards/rest/games/{gameId}/decks/{deckId}     | POST   | -                                                             | The modified game                            |
| Retrieve all player in a game     | /deckofcards/rest/games/{gameId}/players/           |        |                                                               |                                              |
| Add a player to a game            | /deckofcards/rest/games/{gameId}/players/{playerId} | POST   | -                                                             | The modified game                            |
| Remove a player from a game       | /deckofcards/rest/games/{gameId}/players/{playerId} | DELETE | -                                                             | The modified game Note: Player -> Unassigned |
| Retrieve ordered game deck (shoe) | /deckofcards/rest/games/{gameId}/shoe/              | GET    | -                                                             | Game deck (shoe)                             |
| Shuffle game deck (shoe)          | /deckofcards/rest/games/{gameId}/shoe/              | POST   | {"action":"shuffle"}                                          | Game deck (shoe)                             |
| Deal cards to a player            | /deckofcards/rest/games/{gameId}/shoe/              | POST   | {"action":"deal",  "amount":<amount>,  "playerId":<playerId>} | Game deck (shoe)                             |
| Retrieve all decks (unassigned)   | /deckofcards/rest/decks/                            | GET    | -                                                             | List of decks                                |
| Create a deck                     | /deckofcards/rest/decks/                            | POST   | -                                                             | The new deck                                 |
| Retrieve a deck                   | /deckofcards/rest/decks/{deckId}                    | GET    | -                                                             | A deck                                       |
| Retrieve all players (unassigned) | /deckofcards/rest/players/                          | GET    | -                                                             | List of players                              |
| Create a player                   | /deckofcards/rest/players/                          | POST   | -                                                             | The new player                               |
| Retrieve a player (unassigned)    | /deckofcards/rest/players/{playerId}                | GET    | -                                                             | A player                                     |
```